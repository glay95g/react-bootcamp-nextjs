import styles from '../styles/Home.module.css'

function OtherVacancies({ data }) {
    if (!!data) {

        return <div className={styles.container}>
            <div className={styles.main}>
                <h1 className={styles.title}>
                    Все вакансии
                </h1>
                <div className={styles.grid}>
                    {data.items.map((vacancy) => {
                        return <a href={vacancy.apply_alternate_url} key={vacancy.id} className={styles.card}>
                            <b>{vacancy.name}</b> &nbsp; <i>{vacancy.address?.city}</i>
                        </a>;
                    })}
                </div>
            </div>
        </div>;
    }
    return <div className={styles.container}>
        <div className={styles.description}>Вакансии не нейдены :(
        </div>
    </div>;
}

export async function getServerSideProps() {
    const res = await fetch(`https://api.hh.ru/vacancies?per_page=100&page=0&employer_id=46769`)
    const data = await res.json()

    return { props: { data } }
}

export default OtherVacancies;