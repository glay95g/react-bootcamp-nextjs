import React from 'react';
import withPrivateRoute from '../components/withPrivateRoute';
import styles from '../styles/Home.module.css'

const PrivateVacancies = ({userAuth}) => {
    return userAuth && <div className={styles.main}>
        <div className={styles.description}>На данный момент вакансий нет :(
        </div>
    </div>;
};

export default withPrivateRoute(PrivateVacancies);