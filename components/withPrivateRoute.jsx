/* eslint-disable import/no-anonymous-default-export */
import React from 'react';
import { getSession, signIn } from 'next-auth/client'


export default WrappedComponent => {
    const hocComponent = ({ ...props }) => <WrappedComponent {...props} />;

    hocComponent.getInitialProps = async (context) => {
        const userAuth = await getSession();

        if (!userAuth) {
            signIn();
        } else if (WrappedComponent.getInitialProps) {
            const wrappedProps = await WrappedComponent.getInitialProps({ ...context, auth: userAuth });
            return { ...wrappedProps, userAuth };
        }

        return { userAuth };
    };

    return hocComponent;
};